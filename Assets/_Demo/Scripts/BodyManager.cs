﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;

public class BodyManager : MonoBehaviour {
    public BodySourceManager sourceManager;

	public GameObject manPrefab;


    private Dictionary<ulong, BodyController> _Bodies = new Dictionary<ulong, BodyController>();
	

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		
	}


    void Update()
    {

        Kinect.Body[] data = sourceManager.GetData();
        if (data == null)
        {
            return;
        }

        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {
                Destroy(_Bodies[trackingId].gameObject);
                _Bodies.Remove(trackingId);
            }
        }

        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

                RefreshBodyObject(body, _Bodies[body.TrackingId]);
            }
        }
    }

    private BodyController CreateBodyObject(ulong id)
    {
		GameObject body = Instantiate(manPrefab);
		body.name = "Body: " + id;

        return body.GetComponent<BodyController>();
    }

    private void RefreshBodyObject(Kinect.Body body, BodyController bodyObject)
    {
		bodyObject.Refresh(body);
    }



}
