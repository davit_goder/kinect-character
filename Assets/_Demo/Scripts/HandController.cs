﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;

public class HandController : MonoBehaviour
{




    public enum which_hand { right, left };
    public which_hand hand;

    [SerializeField]
    GameObject character;

    [SerializeField]
    float rotation_smooth_factor, position_smooth_factor;

    [SerializeField]
    Transform ik_target;

    [SerializeField]
    Vector3 curr_velo;

    public float handPositionThreshHold;

    private Kinect.HandState state;


    [SerializeField]
    private bool isGrabbing;
    [SerializeField]
    private bool isTouching;

    private MeshRenderer meshRend;
    private Collider col;


    public delegate void Contact(Transform parent, GameObject grabbedObject);
    public event Contact OnGrab;
    public event Contact OnRelease;




    private Vector3 lastPos;
    private Vector3 deltaPos;
    private GameObject grabbedObject;


    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        col = GetComponent<Collider>();
        meshRend = GetComponent<MeshRenderer>();
    }



    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>

    public Vector3 offset;
    void Update()
    {

        ik_target.transform.position = Vector3.SmoothDamp(ik_target.transform.position, transform.position, ref curr_velo, Time.deltaTime * position_smooth_factor);
        // ik_target.transform.rotation = Quaternion.Slerp( Quaternion.Euler(ik_target.transform.rotation.eulerAngles + offset),
        //Quaternion.Euler(transform.rotation.eulerAngles + offset), rotation_smooth_factor);
        //Quaternion.Euler(transform.rotation.eulerAngles + offset), Time.deltaTime * rotation_smooth_factor);
        //Quaternion.Lerp(ik_target.rotation, transform.rotation, rotation_smooth_factor);
        if (state == Kinect.HandState.Closed)
        {
            if (!isGrabbing && OnGrab != null)
            {
                OnGrab(transform, grabbedObject);
            }
            isGrabbing = true;
        }
        else
        {
            if (isGrabbing && OnRelease != null)
            {

                OnRelease(transform, grabbedObject);
                grabbedObject = null;
            }
            isGrabbing = false;
        }
        deltaPos = this.transform.position - lastPos;
        if (deltaPos.magnitude > handPositionThreshHold)
            lastPos = this.transform.position;
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Draggable")
        {
            isTouching = true;
            grabbedObject = other.gameObject;
        }
    }

    /// <summary>
    /// OnTriggerExit is called when the Collider other has stopped touching the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Draggable")
            isTouching = false;

    }


    // DEMO



    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        OnGrab += grab;
        OnRelease += rel;
    }



    void grab(Transform t, GameObject g)
    {

        //grab animation
        if (hand == which_hand.left)
        {
            character.GetComponent<Animator>().SetTrigger("SquizzeLeft");
        }
        else if (hand == which_hand.right)
        {
            character.GetComponent<Animator>().SetTrigger("SquizzeRight");
        }

        Debug.Log("Grab");
        meshRend.material.color = Color.green;
        if (g == null || g.GetComponent<Rigidbody>() == null)
        {
            // col.isTrigger = false;

        }
        else
        {
            var rig = g.GetComponent<Rigidbody>();
            g.transform.parent = t;
            rig.useGravity = false;
            rig.velocity = Vector3.zero;
        }
    }
    void rel(Transform t, GameObject g)
    {
		//release animation
        if (hand == which_hand.left)
        {
            character.GetComponent<Animator>().SetTrigger("ReleaseLeft");
		
        }
        else if (hand == which_hand.right)
        {
            character.GetComponent<Animator>().SetTrigger("ReleaseRight");
        }

        col.isTrigger = true;

        Debug.Log("Rel");
        meshRend.material.color = Color.red;// = red;

        if (g == null || g.GetComponent<Rigidbody>() == null) return;
        g.transform.parent = null;
        var rig = g.GetComponent<Rigidbody>();
        rig.useGravity = true;

        rig.AddForce(deltaPos, ForceMode.Impulse);
    }

    internal void SetState(Kinect.HandState handState)
    {
        state = handState;
    }
}
