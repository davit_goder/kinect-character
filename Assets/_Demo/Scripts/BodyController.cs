﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;


[System.Serializable]
public class AvatarJoint
{
    public Kinect.JointType jointType;
    public Transform jointTransform;
}

public class BodyController : MonoBehaviour
{
	public float scale = 1f;

    public HandController leftHandController;
    public HandController rightHandController;

    public List<AvatarJoint> avatarJoints;

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public void Refresh(Kinect.Body body)
    {

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            var jointo = avatarJoints.Find(x => x.jointType == jt);
            if (jointo == null) continue;
            // Transform jointObj = jointo.jointTransform;
            jointo.jointTransform.position = GetVector3FromJoint(sourceJoint, scale);

            // var orientation = body.JointOrientations[jt].Orientation;
            // jointo.jointTransform.rotation = orientation.;

            // float rotationX = orientation.Pitch();
            // float rotationY = orientation.Yaw();
            // float rotationZ = orientation.Roll();
            // jointo.jointTransform.localEulerAngles = new Vector3(rotationX, rotationY, rotationZ);

            Quaternion rotation = new Quaternion(body.JointOrientations[jt].Orientation.X,
                body.JointOrientations[jt].Orientation.Y,
                body.JointOrientations[jt].Orientation.Z,
                body.JointOrientations[jt].Orientation.W);

            jointo.jointTransform.rotation = new Quaternion(rotation.x, -rotation.y, -rotation.z, rotation.w);

            rightHandController.SetState(body.HandRightState);
            leftHandController.SetState(body.HandLeftState);

        }
    }

	
    private Vector3 GetVector3FromJoint(Kinect.Joint joint, float scale)
    {
        return new Vector3(-joint.Position.X * scale, joint.Position.Y * scale, joint.Position.Z * scale);

    }

}
