﻿using System;
using UnityEngine;
using Windows.Kinect;

public static class JointOrientationExtensions
    {
        /// <summary>
        /// Rotates the specified quaternion around the X axis.
        /// </summary>
        /// <param name="quaternion">The orientation quaternion.</param>
        /// <returns>The rotation in degrees.</returns>
        public static float Pitch(this Windows.Kinect.Vector4 quaternion)
        {
            float value1 = 2.0f * (quaternion.W * quaternion.X + quaternion.Y * quaternion.Z);
            float value2 = 1.0f - 2.0f * (quaternion.X * quaternion.X + quaternion.Y * quaternion.Y);

            float roll = Mathf.Atan2(value1, value2);

            return roll * (180.0f / Mathf.PI);
        }

        /// <summary>
        /// Rotates the specified quaternion around the Y axis.
        /// </summary>
        /// <param name="quaternion">The orientation quaternion.</param>
        /// <returns>The rotation in degrees.</returns>
        public static float Yaw(this Windows.Kinect.Vector4 quaternion)
        {
            float value = 2.0f * (quaternion.W * quaternion.Y - quaternion.Z * quaternion.X);
            value = value > 1.0f ? 1.0f : value;
            value = value < -1.0f ? -1.0f : value;

            float pitch = Mathf.Asin(value);

            return pitch * (180.0f / Mathf.PI);
        }

        /// <summary>
        /// Rotates the specified quaternion around the Z axis.
        /// </summary>
        /// <param name="quaternion">The orientation quaternion.</param>
        /// <returns>The rotation in degrees.</returns>
        public static float Roll(this Windows.Kinect.Vector4 quaternion)
        {
            float value1 = 2.0f * (quaternion.W * quaternion.Z + quaternion.X * quaternion.Y);
            float value2 = 1.0f - 2.0f * (quaternion.Y * quaternion.Y + quaternion.Z * quaternion.Z);

            float yaw = Mathf.Atan2(value1, value2);

            return yaw * (180.0f / Mathf.PI);
        }
    }