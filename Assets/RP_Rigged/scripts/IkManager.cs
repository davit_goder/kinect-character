﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Animator))] 

public class IkManager : MonoBehaviour {
    
    protected Animator animator;
    
    public bool ikActive = false;


    public Transform rightHandObj = null;
    public Transform leftHandObj = null;

    public Transform rightFootObj = null;
    public Transform leftFootObj = null;

    public Transform lookObj = null;

    void Start () 
    {
        animator = GetComponent<Animator>();
    }

	private void FixedUpdate() {
	if(Input.GetKeyDown(KeyCode.L)){
    move_left();
}
if(Input.GetKeyDown(KeyCode.R)){
    move_right();
}
	
	}
    
    //a callback for calculating IK

 [SerializeField]
 Vector3 r_rotation, l_rotation;

    void OnAnimatorIK()
    {
        if(animator) {
            
            //if the IK is active, set the position and rotation directly to the goal. 
            if(ikActive) {

                // Set the look target position, if one has been assigned
                if(lookObj != null) {
                    animator.SetLookAtWeight(1);
                    animator.SetLookAtPosition(lookObj.position);
                }    


                // Set the right hand target position and rotation, if one has been assigned
                if(rightHandObj != null) {
                    animator.SetIKPositionWeight(AvatarIKGoal.RightHand,1);
                    animator.SetIKRotationWeight(AvatarIKGoal.RightHand,1);  
                    animator.SetIKPosition(AvatarIKGoal.RightHand,rightHandObj.position);
                    animator.SetIKRotation( AvatarIKGoal.RightHand, Quaternion.Euler(rightHandObj.rotation.eulerAngles + r_rotation)  );

                }    

				if(leftHandObj != null)
				{
					animator.SetIKPositionWeight(AvatarIKGoal.LeftHand,1);
  					animator.SetIKRotationWeight(AvatarIKGoal.LeftHand,1);  
                    animator.SetIKPosition(AvatarIKGoal.LeftHand,leftHandObj.position);
                    animator.SetIKRotation(AvatarIKGoal.LeftHand,Quaternion.Euler(leftHandObj.rotation.eulerAngles + l_rotation));
				}    

                if(rightFootObj != null) {
                    animator.SetIKPositionWeight(AvatarIKGoal.RightFoot,1);
                    animator.SetIKRotationWeight(AvatarIKGoal.RightFoot,1);  
                    animator.SetIKPosition(AvatarIKGoal.RightFoot,rightFootObj.position);
                     animator.SetIKRotation( AvatarIKGoal.RightFoot, Quaternion.Euler(rightFootObj.rotation.eulerAngles + r_rotation)  );

                }    

				if(leftFootObj != null)
				{
					animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot,1);
  					animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot,1);  
                    animator.SetIKPosition(AvatarIKGoal.LeftFoot,leftFootObj.position);
                    animator.SetIKRotation(AvatarIKGoal.LeftFoot,Quaternion.Euler(leftFootObj.rotation.eulerAngles + l_rotation));
				}    
                
            }
            
            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else {          
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand,0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand,0); 
                animator.SetLookAtWeight(0);
            }
        }
    }    
   public void move_right()
    {
GetComponent<Animator>().SetTrigger("right");
    }

   public void move_left()
    {
GetComponent<Animator>().SetTrigger("left");

    }

}
